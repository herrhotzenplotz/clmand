/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <limits.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#define CL_TARGET_OPENCL_VERSION 110
#include <CL/cl.h>

struct job_descr {
    double center_real, center_imaginary, magnify, color_distortion;
    int    width, height, bailout;
};

static void
err(int exit_code, const char *fmt, ...)
{
    va_list vp;
    va_start(vp, fmt);

    vfprintf(stderr, fmt, vp);
    fprintf(stderr, ": %s\n", strerror(errno));
    va_end(vp);
    exit(exit_code);
}

static void
errx(int exit_code, const char *fmt, ...)
{
    va_list vp;
    va_start(vp, fmt);

    vfprintf(stderr, fmt, vp);
    fputc('\n', stderr);
    va_end(vp);
    exit(exit_code);
}

static cl_device_id
create_device(void)
{
    cl_platform_id platform;
    cl_device_id   dev;
    int		   error;

    error = clGetPlatformIDs(1, &platform, NULL);
    if(error < 0)
        err(error, "Couldn't identify a platform");

    error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev, NULL);
    if(error == CL_DEVICE_NOT_FOUND) {
        error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &dev, NULL);
    }

    if(error < 0)
        err(error, "Couldn't access any devices");

    return dev;
}

static void
usage(void)
{
    fprintf(stderr, "usage: mand [-w width] [-h height] [-b bailout] [-c real:imaginary] [-m magnify] [-d color-distortion]\n");
    fprintf(stderr, "OPTIONS:\n");
    fprintf(stderr, "     -w/-h       width and height of the window in pixels\n");
    fprintf(stderr, "     -m          magnification factor. only the width is distorted\n");
    fprintf(stderr, "                 according to the aspect provided by -h/-w.\n");
    fprintf(stderr, "     -d          distortion of the color map. iteration percentage is taken\n");
    fprintf(stderr, "                 to the n-th root before the color map is applied\n");
    fprintf(stderr, "     -c          center point of the plot\n");
    fprintf(stderr, "     -b          iteration bailout for the mandelbrot kernel\n");
    exit(1);
}

static void
parse_range_arg(double *lower, double *higher)
{
    char *endptr;
    *lower = strtod(optarg, &endptr);

    if (endptr[0] != ':')
        errx(1, "Expected : in range spec");

    *higher = strtod(endptr + 1, NULL);
}

struct q;

struct q_item {
    struct job_descr *descr;
    int               idx;
    int              *output_buffer;
};

struct q {
    struct q_item    *items;
    size_t            items_capacity;
    _Atomic(size_t)   read_idx;
    _Atomic(size_t)   write_idx;
    _Atomic(size_t)   maxread_idx;
    pthread_t        *thread_pool;
    size_t            thread_pool_size;
    bool              done;
};

void
q_init(struct q *q, size_t threads, void *(*kernel)(void *))
{
    q->thread_pool_size = threads;
    q->items_capacity   = 10;
    q->items            = calloc(sizeof(struct q_item), q->items_capacity);
    q->read_idx         = ATOMIC_VAR_INIT(0);
    q->write_idx        = ATOMIC_VAR_INIT(0);
    q->maxread_idx     = ATOMIC_VAR_INIT(0);

    q->thread_pool = calloc(sizeof(pthread_t), threads);

    if (!q->thread_pool)
        err(1, "calloc");

    for (size_t i = 0; i < threads; ++i) {
        if (pthread_create(&q->thread_pool[i], NULL, kernel, q))
            errx(1, "Unable to start thread");
    }
}

void
q_finalise_and_destroy(struct q *q)
{
    q->done = true;

    // Join all the threads
    for (size_t i = 0; i < q->thread_pool_size; ++i) {
        if (pthread_join(q->thread_pool[i], NULL))
            errx(1, "pthread_join");

        printf("INFO : Joined thread %zu\n", i);
    }

    // Destroy all the buffers now that all threads are dead
    free(q->thread_pool);
    free(q->items);
}

int
q_dq(struct q *q, struct q_item *out)
{
    size_t next_read, curr_read, curr_maxread;

top:
    do {
        curr_read    = atomic_load(&q->read_idx);
        curr_maxread = atomic_load(&q->maxread_idx);

        if (curr_read == curr_maxread) {
            if (q->done)
                return 0;

            goto top;
        }

        *out = q->items[curr_read];

        next_read = (curr_read + 1) % q->items_capacity;
    } while (!atomic_compare_exchange_weak(&q->read_idx, &curr_read, next_read));

    return 1;
}

void
q_nq(struct q *q, struct q_item it)
{
    size_t curr_write, curr_read, next_write;

top:
    do {
        curr_write = atomic_load(&q->write_idx);
        curr_read  = atomic_load(&q->read_idx);
        next_write = (curr_write + 1) % q->items_capacity;

        // queue is full
        if (next_write == curr_read)
            goto top;

    } while (!atomic_compare_exchange_weak(&q->write_idx, &curr_write, next_write));
    // If the atomic cmpxchg fails, this means that another producer
    // wrote. Thus try again.

    // Enqueue item and advance queue
    q->items[curr_write] = it;

    while (!atomic_compare_exchange_weak(&q->maxread_idx, &curr_write, next_write))
        pthread_yield();
}

static inline double
mix(double lower, double higher, double val)
{
    return lower + (higher - lower) * val;
}

static void *
mand_kernel(void *_q)
{
    struct q      *q         = _q;
    struct q_item  work_item = {0};

    while (q_dq(q, &work_item)) {

        // This code is exactly the same as in mandelbrot.cl
        int x_px = work_item.idx % work_item.descr->width;
        int y_px = work_item.idx / work_item.descr->width;

        double aspect     = (double)work_item.descr->width / (double)work_item.descr->height;
        double mag_factor = 1.0 / work_item.descr->magnify;

        double x0 = mix(work_item.descr->center_real - 0.5 * mag_factor * aspect,
                        work_item.descr->center_real + 0.5 * mag_factor * aspect,
                        (double)(x_px) / (double)(work_item.descr->width));
        double y0 = mix(work_item.descr->center_imaginary - 0.5 * mag_factor,
                        work_item.descr->center_imaginary + 0.5 * mag_factor,
                        (double)(y_px) / (double)(work_item.descr->height));

        double x = 0, y = 0;

        int iter = 0;

        while (x * x + y * y <= 4 && iter < work_item.descr->bailout) {
            double tmp = x * x - y * y + x0;
            y = 2 * x * y + y0;
            x = tmp;
            iter += 1;
        }

        work_item.output_buffer[work_item.idx] = iter;
    }

    return NULL;
}

int
main(int argc, char *argv[])
{
    cl_device_id      device_id;
    cl_context        device_context;
    cl_command_queue  device_queue;
    cl_program        program;
    cl_kernel         mbrot_kernel;
    cl_mem            iter_buffer;
    int               error_code, *gpu_buffer;
    size_t            global_size, local_size, cpu_threads;
    struct timespec   timer_begin, timer_end;

    struct job_descr descr = {
        .center_real      = -1.0,
        .center_imaginary = 0,
        .magnify          = .5,
        .width            = 1920,
        .height           = 1080,
        .bailout          = 500,
        .color_distortion = 3.2,
    };

    char ch;
    while ((ch = getopt(argc, argv, "w:h:b:c:m:d:t:")) != -1) {
        switch (ch) {
        case 'w':
            descr.width = strtol(optarg, NULL, 10);
            break;
        case 'h':
            descr.height = strtol(optarg, NULL, 10);
            break;
        case 'b':
            descr.bailout = strtol(optarg, NULL, 10);
            break;
        case 'c':
            parse_range_arg(&descr.center_real, &descr.center_imaginary);
            break;
        case 'm':
            descr.magnify = strtod(optarg, NULL);
            break;
        case 'd':
            descr.color_distortion = strtod(optarg, NULL);
            break;
        case 't':
            cpu_threads = strtol(optarg, NULL, 10);
            break;
        case '?':
        default:
            usage();
        }
    }
    argc -= optind;
    argv += optind;

    /******************************/
    /*  CL INITIALIZATION         */
    /******************************/

    device_id      = create_device();
    device_context = clCreateContext(NULL, 1, &device_id, NULL, NULL, NULL);
    device_queue   = clCreateCommandQueue(device_context, device_id, 0, NULL);

    int fd = open("src/mandelbrot.cl", O_RDONLY);
    if (fd < 0)
        err(1, "open");

    struct stat st = {0};
    if (fstat(fd, &st) < 0)
        err(1, "fstat");

    const char *src = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (src == MAP_FAILED)
        err(1, "mmap");

    program = clCreateProgramWithSource(device_context, 1, &src, (const size_t *)&st.st_size, NULL);
    clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    {
        size_t log_size;
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
        char *program_log = (char*) malloc(log_size);
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG,
                              log_size + 1, program_log, NULL);
        printf("%.*s\n", (int)log_size, program_log);
        free(program_log);
    }

    munmap((void *)(src), st.st_size);
    close(fd);

    mbrot_kernel = clCreateKernel(program, "mbrot", &error_code);
    if (error_code < 0)
        errx(1, "Failed to create mbrot kernel: %d", error_code);

    /* Buffer setup */
    iter_buffer = clCreateBuffer(device_context, CL_MEM_READ_WRITE,
                                 descr.width * descr.height * sizeof(int),
                                 NULL, NULL);
    if (error_code < 0)
        errx(1, "clCreateBuffer(): %d", error_code);


    global_size = descr.width * descr.height;
    local_size  = 64;

    clSetKernelArg(mbrot_kernel, 0, sizeof(descr),       &descr);
    clSetKernelArg(mbrot_kernel, 1, sizeof(iter_buffer), &iter_buffer);

    puts("INFO : Starting timer");
    if (clock_gettime(CLOCK_MONOTONIC, &timer_begin) < 0)
        err(1, "clock_gettime");

    puts("INFO : Enqueuing mandelbrot kernel");
    error_code = clEnqueueNDRangeKernel(device_queue, mbrot_kernel, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
    if (error_code < 0)
        errx(1, "unable to enqueue kernel: %d", error_code);

    puts("INFO : Downloading data back to host");
    gpu_buffer = calloc(sizeof(int), global_size);
    error_code = clEnqueueReadBuffer(device_queue, iter_buffer, CL_TRUE, 0, descr.width * descr.height * sizeof(int), gpu_buffer, 0, NULL, NULL);
    if (error_code < 0)
        errx(1, "unable to read back image to host: %d", error_code);

    clFinish(device_queue);

    puts("INFO : Stopping timers");
    if (clock_gettime(CLOCK_MONOTONIC, &timer_end) < 0)
        err(1, "clock_gettime");

    double gpu_diff = (timer_end.tv_sec - timer_begin.tv_sec) + (double)(timer_end.tv_nsec - timer_begin.tv_nsec) / (double)1e9;

    clReleaseMemObject(iter_buffer);
    clReleaseKernel(mbrot_kernel);
    clReleaseProgram(program);
    clReleaseCommandQueue(device_queue);
    clReleaseContext(device_context);

    printf("INFO : Performing the same computation on the host CPU with %zu threads\n", cpu_threads);
    puts  ("WARN : Thrash thrash thrash etc...");

    struct q       q           = {0};
    struct q_item  item        = {0};
    int           *host_buffer = calloc(sizeof(int), global_size);
    int            errors      = 0;

    puts  ("INFO : Initializing queue");
    q_init(&q, cpu_threads, mand_kernel);

    item.descr         = &descr;
    item.output_buffer = host_buffer;

    puts("INFO : Starting timer");
    if (clock_gettime(CLOCK_MONOTONIC, &timer_begin) < 0)
        err(1, "clock_gettime");

    puts  ("INFO : Queueing up data");
    for (size_t i = 0; i < global_size; ++i) {
        if (i % 1000 == 0) {
            printf("\r     : item %05zu / %05zu", i, global_size);
            fflush(stdout);
        }
        item.idx = i;
        q_nq(&q, item);
    }

    puts  ("INFO : Finishing computation");
    q_finalise_and_destroy(&q);

    puts("INFO : Stopping timers");
    if (clock_gettime(CLOCK_MONOTONIC, &timer_end) < 0)
        err(1, "clock_gettime");

    double cpu_diff = (timer_end.tv_sec - timer_begin.tv_sec) + (double)(timer_end.tv_nsec - timer_begin.tv_nsec) / (double)1e9;

    int max_delta = INT_MIN;
    int min_delta = INT_MAX;
    double avrg   = 0;

    for (size_t i = 0; i < global_size; ++i) {
        if (gpu_buffer[i] != host_buffer[i]) {

            int delta = abs(gpu_buffer[i] - host_buffer[i]);

            printf("FAIL : at index %010zu : Host = %010d, Device = %010d. DIFF = %010d\n",
                   i, gpu_buffer[i], host_buffer[i], delta);
            errors += 1;

            if (delta > max_delta)
                max_delta = delta;

            if (delta < min_delta)
                min_delta = delta;

            avrg += (double)delta / (double)global_size;
        }
    }

    double error_rate = (double)(errors) / (double)(global_size);

    printf("INFO : Done. There were %d errors and %lu correct values.\n"
           "     : That makes an error rate of %e\n"
           "     : The average delta is .......%e.\n"
           "     : Maximum delta is ...........%d.\n"
           "     : Minimum delta is ...........%d.\n",
           errors, global_size-errors, error_rate,avrg, max_delta, min_delta);
    printf("INFO : Total execution time on GPU : %.6lf seconds\n", gpu_diff);
    puts  ("     : Please note that this time includes the time spent on IO to the console");
    printf("INFO : Total execution time on CPU : %.6lf seconds\n", cpu_diff);
    puts  ("     : Please note that this time also includes the time spent on IO to the console");


    free(host_buffer);
    free(gpu_buffer);

    return 0;
}
