/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include <SDL2/SDL.h>

#define CL_TARGET_OPENCL_VERSION 110
#include <CL/cl.h>

struct job_descr {
    double center_real, center_imaginary, magnify, color_distortion;
    int    width, height, bailout;
};

static void
err(int exit_code, const char *fmt, ...)
{
    va_list vp;
    va_start(vp, fmt);

    vfprintf(stderr, fmt, vp);
    fprintf(stderr, ": %s\n", strerror(errno));
    va_end(vp);
    exit(exit_code);
}

static void
errx(int exit_code, const char *fmt, ...)
{
    va_list vp;
    va_start(vp, fmt);

    vfprintf(stderr, fmt, vp);
    fputc('\n', stderr);
    va_end(vp);
    exit(exit_code);
}

static cl_device_id
create_device(void)
{
    cl_platform_id platform;
    cl_device_id   dev;
    int		   error;

    error = clGetPlatformIDs(1, &platform, NULL);
    if(error < 0)
        err(error, "Couldn't identify a platform");

    error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev, NULL);
    if(error == CL_DEVICE_NOT_FOUND) {
        error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &dev, NULL);
    }

    if(error < 0)
        err(error, "Couldn't access any devices");

    return dev;
}

static void
usage(void)
{
    fprintf(stderr, "usage: mand [-w width] [-h height] [-b bailout] [-c real:imaginary] [-m magnify] [-d color-distortion]\n");
    fprintf(stderr, "OPTIONS:\n");
    fprintf(stderr, "     -w/-h       width and height of the window in pixels\n");
    fprintf(stderr, "     -m          magnification factor. only the width is distorted\n");
    fprintf(stderr, "                 according to the aspect provided by -h/-w.\n");
    fprintf(stderr, "     -d          distortion of the color map. iteration percentage is taken\n");
    fprintf(stderr, "                 to the n-th root before the color map is applied\n");
    fprintf(stderr, "     -c          center point of the plot\n");
    fprintf(stderr, "     -b          iteration bailout for the mandelbrot kernel\n");
    exit(1);
}

static void
parse_range_arg(double *lower, double *higher)
{
    char *endptr;
    *lower = strtod(optarg, &endptr);

    if (endptr[0] != ':')
        errx(1, "Expected : in range spec");

    *higher = strtod(endptr + 1, NULL);
}

int
main(int argc, char *argv[])
{
    cl_device_id      device_id;
    cl_context        device_context;
    cl_command_queue  device_queue;
    cl_program        program;
    cl_kernel         mbrot_kernel, colorize_kernel;
    cl_mem            iter_buffer, image_buffer;
    SDL_Renderer     *renderer;
    SDL_Window       *window;
    SDL_Surface      *surface;
    SDL_Texture      *texture;
    int               error_code;
    size_t            global_size, local_size;
    struct timespec   timer_begin, timer_end;

    struct job_descr descr = {
        .center_real      = -1.0,
        .center_imaginary = 0,
        .magnify          = .5,
        .width            = 1920,
        .height           = 1080,
        .bailout          = 500,
        .color_distortion = 3.2,
    };

    char ch;
    while ((ch = getopt(argc, argv, "w:h:b:c:m:d:")) != -1) {
        switch (ch) {
        case 'w':
            descr.width = strtol(optarg, NULL, 10);
            break;
        case 'h':
            descr.height = strtol(optarg, NULL, 10);
            break;
        case 'b':
            descr.bailout = strtol(optarg, NULL, 10);
            break;
        case 'c':
            parse_range_arg(&descr.center_real, &descr.center_imaginary);
            break;
        case 'm':
            descr.magnify = strtod(optarg, NULL);
            break;
        case 'd':
            descr.color_distortion = strtod(optarg, NULL);
            break;
        case '?':
        default:
            usage();
        }
    }
    argc -= optind;
    argv += optind;


    /******************************/
    /*  SDL INITIALIZATION        */
    /******************************/

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        errx(1, "SDL_Init(): %s", SDL_GetError());

    if (SDL_CreateWindowAndRenderer(descr.width, descr.height,
                                    SDL_WINDOW_RESIZABLE,
                                    &window, &renderer) < 0)
        errx(1, "SDL_CreateWindowAndRenderer(): %s", SDL_GetError());

    SDL_SetWindowTitle(window, "CLMand");

    /* XXX: This assumes little endianess */
    surface = SDL_CreateRGBSurface(0, descr.width, descr.height, 32,
                                   0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000);
    if (!surface)
        errx(1, "SDLCreateRGBSurface(): %s", SDL_GetError());


    /******************************/
    /*  CL INITIALIZATION         */
    /******************************/

    device_id      = create_device();
    device_context = clCreateContext(NULL, 1, &device_id, NULL, NULL, NULL);
    device_queue   = clCreateCommandQueue(device_context, device_id, 0, NULL);

    int fd = open("src/mandelbrot.cl", O_RDONLY);
    if (fd < 0)
        err(1, "open");

    struct stat st = {0};
    if (fstat(fd, &st) < 0)
        err(1, "fstat");

    const char *src = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (src == MAP_FAILED)
        err(1, "mmap");

    program = clCreateProgramWithSource(device_context, 1, &src, (const size_t *)&st.st_size, NULL);
    clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    {
        size_t log_size;
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
        char *program_log = (char*) malloc(log_size);
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG,
                              log_size + 1, program_log, NULL);
        printf("%.*s\n", (int)log_size, program_log);
        free(program_log);
    }

    munmap((void *)(src), st.st_size);
    close(fd);

    mbrot_kernel = clCreateKernel(program, "mbrot", &error_code);
    if (error_code < 0)
        errx(1, "Failed to create mbrot kernel: %d", error_code);

    colorize_kernel = clCreateKernel(program, "render", &error_code);
    if (error_code < 0)
        errx(1, "Failed to create bw kernel: %d", error_code);

    /* Buffer setup */
    iter_buffer = clCreateBuffer(device_context, CL_MEM_READ_WRITE,
                                 descr.width * descr.height * sizeof(int),
                                 NULL, NULL);
    if (error_code < 0)
        errx(1, "clCreateBuffer(): %d", error_code);

    image_buffer = clCreateBuffer(device_context, CL_MEM_WRITE_ONLY,
                                  descr.width * descr.height * sizeof(Uint32),
                                  NULL, &error_code);
    if (error_code < 0)
        errx(1, "clCreateBuffer(): %d", error_code);

    global_size = descr.width * descr.height;
    local_size  = 64;

    clSetKernelArg(mbrot_kernel, 0, sizeof(descr),       &descr);
    clSetKernelArg(mbrot_kernel, 1, sizeof(iter_buffer), &iter_buffer);

    puts("INFO : Starting timer");
    if (clock_gettime(CLOCK_MONOTONIC, &timer_begin) < 0)
        err(1, "clock_gettime");

    puts("INFO : Enqueuing mandelbrot kernel");
    error_code = clEnqueueNDRangeKernel(device_queue, mbrot_kernel, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
    if (error_code < 0)
        errx(1, "unable to enqueue kernel: %d", error_code);

    clSetKernelArg(colorize_kernel, 0, sizeof(descr),       &descr);
    clSetKernelArg(colorize_kernel, 1, sizeof(iter_buffer), &iter_buffer);
    clSetKernelArg(colorize_kernel, 2, sizeof(image_buffer), &image_buffer);

    puts("INFO : Enqueuing rendering kernel");
    error_code = clEnqueueNDRangeKernel(device_queue, colorize_kernel, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
    if (error_code < 0)
        errx(1, "unable to enqueue kernel: %d", error_code);

    SDL_LockSurface(surface);
    puts("INFO : Downloading rendered data back to host");
    error_code = clEnqueueReadBuffer(device_queue, image_buffer, CL_TRUE, 0, descr.width * descr.height * sizeof(Uint32), surface->pixels, 0, NULL, NULL);
    if (error_code < 0)
        errx(1, "unable to read back image to host: %d", error_code);

    clFinish(device_queue);

    puts("INFO : Stopping timers");
    if (clock_gettime(CLOCK_MONOTONIC, &timer_end) < 0)
        err(1, "clock_gettime");

    double diff = (timer_end.tv_sec - timer_begin.tv_sec) + (double)(timer_end.tv_nsec - timer_begin.tv_nsec) / (double)1e9;

    printf("INFO : Total execution time : %.6lf seconds\n", diff);
    puts  ("     : Please note that this time includes the time spent on IO to the console");
    puts  ("     : I will now pass the rendered buffer to SDL");

    SDL_UnlockSurface(surface);

    clReleaseMemObject(iter_buffer);
    clReleaseMemObject(image_buffer);
    clReleaseKernel(mbrot_kernel);
    clReleaseKernel(colorize_kernel);
    clReleaseProgram(program);
    clReleaseCommandQueue(device_queue);
    clReleaseContext(device_context);

    texture = SDL_CreateTextureFromSurface(renderer, surface);
    if (!texture)
        errx(1, "SDL_CreateTextureFromSurface: %s", SDL_GetError());

    int    fps         = 60;
    double delay_s     = 1 / (double)fps,
           delay_ms    = delay_s * 1e3;
    int    should_quit = 0;

    while (!should_quit) {
        SDL_Event event;

        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                should_quit = 1;
                break;
            }
        }

        SDL_RenderCopy(renderer, texture, NULL, NULL);
        SDL_RenderPresent(renderer);
        SDL_Delay((int)delay_ms);
    }

    SDL_DestroyTexture(texture);
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);

    SDL_Quit();

    return 0;
}
