.POSIX:

CC	=	/usr/bin/cc
PROG	=	mand
SRCS	=	src/main.c

CFLAGS	=	-std=c11 -g -O0 -pedantic -Wall -Wextra -Werror
# CPPFLAGS=	-D_XOPEN_SOURCE=700

COPTX_CL=	`pkg-config --cflags OpenCL`
COPTX_SDL2=	`pkg-config --cflags sdl2`

LDOPTX_CL=	`pkg-config --libs OpenCL`
LDOPTX_SDL2=	`pkg-config --libs sdl2`

CFLAGS	+=	${COPTX_CL} ${COPTX_SDL2}
LDFLAGS	+=	${LDOPTX_CL} ${LDOPTX_SDL2}

OBJS	=	${SRCS:.c=.o}

.PHONY: all clean
all: ${PROG} ieee754check

ieee754check: src/ieee754check.o
	${CC} ${CFLAGS} ${CPPFLAGS} -o ieee754check src/ieee754check.o ${LDOPTX_CL} -lpthread

${PROG}: ${OBJS}
	${CC} ${CFLAGS} ${CPPFLAGS} -o ${PROG} ${OBJS} ${LDFLAGS}

.c.o:
	${CC} ${CPPFLAGS} ${CFLAGS} -c $< -o $@

clean:
	rm -f ${OBJS} ${PROG} plot.png
